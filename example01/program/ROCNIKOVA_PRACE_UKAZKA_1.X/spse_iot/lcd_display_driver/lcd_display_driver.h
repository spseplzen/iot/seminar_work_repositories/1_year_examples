/*******************************************************************************
  LCD DISPLAY DRIVER

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    lcd_display_driver.h

  Summary:
    LCD display Header File
 
  Version:
    1.1 stable

  Description:
    This file provides basic functions for LCD display control.

*******************************************************************************/


#ifndef LCD_DISPLAY_H
#define LCD_DISPLAY_H


#include "../peripheral/plib_port/port.h"
#include "../easy_delay/easy_delay.h"

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
    extern "C" {
#endif
// DOM-IGNORE-END

        
#define LCD_DISPLAY_DIMENSION_X UINT8_C(16)
#define LCD_DISPLAY_DIMENSION_Y UINT8_C(2)

        

typedef struct lcd_display_descriptor_t lcd_display_t;
typedef struct lcd_display_config_descriptor_t lcd_display_config_t;
typedef struct lcd_display_pins_descriptor_t lcd_display_pins_t;
typedef struct lcd_display_data_pins_descriptor_t lcd_display_data_pins_t;
typedef struct lcd_display_comm_pins_descriptor_t lcd_display_comm_pins_t;
typedef struct lcd_display_position_descriptor_t lcd_display_position_t;
typedef struct lcd_display_entry_mode_descriptor_t lcd_display_entry_mode_t;
typedef struct lcd_display_function_set_descriptor_t lcd_display_function_set_t;
typedef struct lcd_display_control_descriptor_t lcd_display_control_t;


typedef enum{
    LCD_DISPLAY_ENTRY_MODE_INCREMENT_INC     = 0,
    LCD_DISPLAY_ENTRY_MODE_INCREMENT_SOFTINC = 1,
    LCD_DISPLAY_ENTRY_MODE_INCREMENT_NOINC   = 2
} LCD_DISPLAY_ENTRY_MODE_INCREMENT_e;

typedef enum{
    LCD_DISPLAY_ENTRY_MODE_SHIFT_SH   = 0,
    LCD_DISPLAY_ENTRY_MODE_SHIFT_NOSH = 1
} LCD_DISPLAY_ENTRY_MODE_SHIFT_e;

typedef enum{
    LCD_DISPLAY_CONTROL_POWER_OFF = 0,
    LCD_DISPLAY_CONTROL_POWER_ON  = 1
} LCD_DISPLAY_CONTROL_POWER_e;

typedef enum{
    LCD_DISPLAY_CONTROL_CURSOR_OFF = 0,
    LCD_DISPLAY_CONTROL_CURSOR_ON  = 1
} LCD_DISPLAY_CONTROL_CURSOR_e;

typedef enum{
    LCD_DISPLAY_CONTROL_BLINKING_OFF = 0,
    LCD_DISPLAY_CONTROL_BLINKING_ON  = 1
} LCD_DISPLAY_CONTROL_BLINKING_e;

typedef enum{
    LCD_DISPLAY_FUNCTION_SET_DATA_MODE_8b = 0,
    LCD_DISPLAY_FUNCTION_SET_DATA_MODE_4b = 1
} LCD_DISPLAY_FUNCTION_SET_DATA_MODE_e;

typedef enum{
    LCD_DISPLAY_FUNCTION_SET_LINE_MODE_1LINE = 0,
    LCD_DISPLAY_FUNCTION_SET_LINE_MODE_2LINE = 1
} LCD_DISPLAY_FUNCTION_SET_LINE_MODE_e;

typedef enum{
    LCD_DISPLAY_FUNCTION_SET_FONT_SIZE_5X8  = 0,
    LCD_DISPLAY_FUNCTION_SET_FONT_SIZE_5X10 = 1
} LCD_DISPLAY_FUNCTION_SET_FONT_SIZE_e;

struct lcd_display_position_descriptor_t{
    uint8_t x;
    uint8_t y;
};

struct lcd_display_entry_mode_descriptor_t{
    LCD_DISPLAY_ENTRY_MODE_INCREMENT_e increment;
    LCD_DISPLAY_ENTRY_MODE_SHIFT_e shift;
};

struct lcd_display_function_set_descriptor_t{
    LCD_DISPLAY_FUNCTION_SET_DATA_MODE_e data_mode;
    LCD_DISPLAY_FUNCTION_SET_LINE_MODE_e line_mode;
    LCD_DISPLAY_FUNCTION_SET_FONT_SIZE_e font_size;
};

struct lcd_display_control_descriptor_t{
    LCD_DISPLAY_CONTROL_POWER_e power;
    LCD_DISPLAY_CONTROL_CURSOR_e cursor;
    LCD_DISPLAY_CONTROL_BLINKING_e blinking;
};

struct lcd_display_config_descriptor_t{
    lcd_display_function_set_t function_set; // 8/4 bit communication
    lcd_display_entry_mode_t entry_mode;     // increment, shift
    lcd_display_control_t display_control;   // display on/off, cursor on/off, blinking on/off
    lcd_display_position_t dimensions;       // dimensions (x,y)
};

struct lcd_display_data_pins_descriptor_t{
    PORT_PIN_e D0;
    PORT_PIN_e D1;
    PORT_PIN_e D2;
    PORT_PIN_e D3;
    PORT_PIN_e D4;
    PORT_PIN_e D5;
    PORT_PIN_e D6;
    PORT_PIN_e D7;
};

struct lcd_display_comm_pins_descriptor_t{
    PORT_PIN_e EN;
    PORT_PIN_e RS;
};

struct lcd_display_pins_descriptor_t{
    lcd_display_data_pins_t data;
    lcd_display_comm_pins_t comm;
};

struct lcd_display_descriptor_t{
    lcd_display_config_t config;
    lcd_display_pins_t pins;
    lcd_display_position_t pos;
};
        


bool lcd_display_set_cursor(lcd_display_t *me, uint8_t x, uint8_t y);
bool lcd_display_get_cursor(const lcd_display_t const *me, uint8_t *x, uint8_t *y);
bool lcd_display_write_char(lcd_display_t *me, const char c);
bool lcd_display_write_char_xy(lcd_display_t *me, const char c, const uint8_t x, const uint8_t y);
bool lcd_display_write_string(lcd_display_t *me, const char *s);
bool lcd_display_clear_xy(lcd_display_t *me, const uint8_t x, const uint8_t y);
bool lcd_display_clear_row(lcd_display_t *me, const uint8_t y);
bool lcd_display_clear(lcd_display_t *me);
bool lcd_display_init(lcd_display_t *me, const lcd_display_config_t const *cfg, const lcd_display_pins_t const *pins, const uint8_t init_x, const uint8_t init_y);


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

}

#endif
// DOM-IGNORE-END

#endif // LCD_DISPLAY_H
