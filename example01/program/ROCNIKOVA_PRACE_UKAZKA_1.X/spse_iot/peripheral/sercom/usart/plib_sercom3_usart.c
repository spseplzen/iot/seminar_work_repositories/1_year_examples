/*******************************************************************************
  PLIB SERCOM3 USART Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    plib_sercom3_usart.c

  Summary:
    PLIB SERCOM3 USART Source File
 
  Version:
    1.0 stable

  Description:
    This file provides basic functions for USART communication.

*******************************************************************************/


#include "plib_sercom3_usart.h"

#include <xc.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>


// Functions for stdout
static inline int USART_printf(char c, FILE *stream){
    SERCOM3_USART_WriteByte((char) c);
    return 0;
}

// Function for stdin
static inline int USART_scanf(FILE *stream){
    return SERCOM3_USART_ReadByte();
}



void SERCOM3_USART_Initialize(uint16_t baudrate){
    
    PORTB.DIRSET = PIN0_bm;
    PORTB.DIRCLR = PIN1_bm;
    
    PORTMUX.USARTROUTEA &= 0b00111111;
    
    USART3.BAUD = (uint16_t)USART_BAUD_RATE(baudrate); // Set baud rate
    
    USART3.CTRLC =  USART_CHSIZE_0_bm
                    | USART_CHSIZE_1_bm; // Set 8-bit format
    
    
    USART3.CTRLB |= USART_RXEN_bm | USART_TXEN_bm; // Enable RX and TX
    
    stdout = stdin = fdevopen(USART_printf, USART_scanf); // Connect USART to stdin and stdout
}


bool SERCOM3_USART_TransmitterIsReady(){
    return ((USART3.STATUS & USART_DREIF_bm) >> USART_DREIF_bp);
}


void SERCOM3_USART_WriteByte(int data){   
    while (!SERCOM3_USART_TransmitterIsReady()); // Wait for the transmit buffer to be empty
    USART3.TXDATAL = data & 0xFF; // Send data
}


void SERCOM3_USART_Write(void *buffer, const size_t size){
    for(size_t i = 0; i < size; i++)        
        SERCOM3_USART_WriteByte(((char *) buffer)[i]);
}


bool SERCOM3_USART_ReceiverIsReady(){
    return ((USART3.STATUS & USART_RXCIF_bm) >> USART_RXCIF_bp);
}


int SERCOM3_USART_ReadByte(){
    while(!SERCOM3_USART_ReceiverIsReady()); // Wait for data to be received
    return USART3.RXDATAL; // Return received data
}


void SERCOM3_USART_Read(void *buffer, const size_t size){
    for(size_t i = 0; i < size; ++i)
        ((char *) buffer)[i] = SERCOM3_USART_ReadByte();
}
