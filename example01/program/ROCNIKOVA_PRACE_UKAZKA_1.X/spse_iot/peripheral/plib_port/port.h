/*******************************************************************************
  PLIB PORT Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    blib_port.h

  Summary:
    PLIB PORT library Header File
 
  Version:
    1.1 stable

  Description:
    This file provides basic functions for PORT periphery.

*******************************************************************************/


#ifndef PORT_H
#define PORT_H

#include <stdbool.h>
#include <stdint.h>
#include <xc.h>


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
    extern "C" {
#endif
// DOM-IGNORE-END




// *****************************************************************************
// Section: Data types and constants
// *****************************************************************************

/*** Macros for boolean voltage logical level ***/
#define HIGH true
#define LOW  false

/*** Macros for USER_LED pin ***/
#define USER_LED_Set()               (PORTB.OUT = PORTB.OUT | ((uint8_t)1U << 3U))
#define USER_LED_Clear()             (PORTB.OUT = PORTB.OUT & ~((uint8_t)1U << 3U))
#define USER_LED_Toggle()            (PORTB.OUTTGL = (uint8_t)1U << 3U)
#define USER_LED_OutputEnable()      (PORTB.DIR = PORTB.DIR | ((uint8_t)1U << 3U))
#define USER_LED_InputEnable()       (PORTB.DIR = PORTB.DIR & ~((uint8_t)1U << 3U))
#define USER_LED_Get()               (((uint8_t)PORTB.IN >> 3U) & 1U)
#define USER_LED_PIN                  PORT_PIN_PB03

/*** Macros for USER_BUTTON pin ***/
#define USER_BUTTON_Set()               (PORTB.OUT = PORTB.OUT | ((uint8_t)1U << 2U))
#define USER_BUTTON_Clear()             (PORTB.OUT = PORTB.OUT & ~((uint8_t)1U << 2U))
#define USER_BUTTON_Toggle()            (PORTB.OUTTGL = (uint8_t)1U << 2U)
#define USER_BUTTON_OutputEnable()      (PORTB.DIR = PORTB.DIR | ((uint8_t)1U << 2U))
#define USER_BUTTON_InputEnable()       (PORTB.DIR = PORTB.DIR & ~((uint8_t)1U << 2U))
#define USER_BUTTON_Get()               (((uint8_t)PORTB.IN >> 2U) & 1U)
#define USER_BUTTON_PIN                  PORT_PIN_PB02



// *****************************************************************************
/* PORTs

  Summary:
    Identifies the available Ports.

  Description:
    This enumeration identifies all the ports that are available on this device.
*/

typedef enum
{
    PORT_PORTA = 0U, // PORTA
    PORT_PORTB = 1U, // PORTB
    PORT_PORTC = 2U, // PORTC
    PORT_PORTD = 3U, // PORTD
    PORT_PORTE = 4U, // PORTE
    PORT_PORTF = 5U, // PORTF

    PORT_NONE  = 8U
} PORT_e;



// *****************************************************************************
/* PORT Pins

  Summary:
    Identifies the available Ports pins.

  Description:
    This enumeration identifies all the ports pins that are available on this device.
*/

typedef enum{
                         //          Special            ADC0       ACn                         DAC0     OPAMP       ZCDn         USARTn      SPIn        TWIn (I2C)     TCAn            TCBn       TCD0       EVSYS      CCL
                         //         ----------------- | ------- | ------------------------- | ------ | --------- | ---------- | --------- | --------- | ------------ | ------------- | -------- | -------- | -------- | -----------
    PORT_PIN_PA00 = 0U,  // PA0 <--  XTALHF1, EXTCLK  |  x      |  x                        |  x     |  x        |  x         |  0, TxD   |  x        |  x           |  0 WO0        |  x       | x        |  x       |  LUT0, IN0
    PORT_PIN_PA01 = 1U,  // PA1 <--  XTALHF2          |  x      |  x                        |  x     |  x        |  x         |  0, RxD   |  x        |  x           |  0 WO1        |  x       | x        |  x       |  LUT0, IN1
    PORT_PIN_PA02 = 2U,  // PA2 <--  TWI, Fm+         |  x      |  x                        |  x     |  x        |  x         |  0, XCK   |  x        |  0, SDA(MS)  |  0 WO2        |  0 WO    | x        |  EVOUTA  |  LUT0, IN2
    PORT_PIN_PA03 = 3U,  // PA3 <--  TWI, Fm+         |  x      |  x                        |  x     |  x        |  x         |  0, XDIR  |  x        |  0, SCL(MS)  |  0 WO3        |  1 WO    | x        |  x       |  LUT0, OUT
    PORT_PIN_PA04 = 4U,  // PA4 <--  x                |  x      |  x                        |  x     |  x        |  x         |  0, TxD   |  0, MOSI  |  x           |  0 WO4        |  x       | WOA      |  x       |  x
    PORT_PIN_PA05 = 5U,  // PA5 <--  x                |  x      |  x                        |  x     |  x        |  x         |  0, RxD   |  0, MISO  |  x           |  0 WO5        |  x       | WOB      |  x       |  x
    PORT_PIN_PA06 = 6U,  // PA6 <--  x                |  x      |  x                        |  x     |  x        |  x         |  0, XCK   |  0, SCK   |  x           |  x            |  x       | WOC      |  x       |  LUT0, OUT
    PORT_PIN_PA07 = 7U,  // PA7 <--  CLKOUT           |  x      |  0,1,2 OUT                |  x     |  x        |  0,1,2 OUT |  0, XDIR  |  0, !SS   |  x           |  x            |  x       | WOD      |  EVOUTA  |  x

    PORT_PIN_PB00 = 8U,  // PB0 <--  x                |  x      |  x                        |  x     |  x        |  x         |  3, TxD   |  x        |  x           |  0,1 WO0      |  x       | x        |  x       |  LUT4, IN0
    PORT_PIN_PB01 = 9U,  // PB1 <--  x                |  x      |  x                        |  x     |  x        |  x         |  3, RxD   |  x        |  x           |  0,1 WO1      |  x       | x        |  x       |  LUT4, IN1
    PORT_PIN_PB02 = 10U, // PB2 <--  TWI              |  x      |  x                        |  x     |  x        |  x         |  3, XCK   |  x        |  1, SDA(MS)  |  0,1 WO2      |  x       | x        |  EVOUTB  |  LUT4, IN2
    PORT_PIN_PB03 = 11U, // PB3 <--  TWI              |  x      |  x                        |  x     |  x        |  x         |  3, XDIR  |  x        |  1, SCK(MS)  |  0,1 WO3      |  x       | x        |  x       |  LUT4, OUT
    PORT_PIN_PB04 = 12U, // PB4 <--  x                |  x      |  x                        |  x     |  x        |  x         |  3, TxD   |  1, MOSI  |  x           |  0,1 WO4      |  2 WO    | WOA      |  x       |  x
    PORT_PIN_PB05 = 13U, // PB5 <--  x                |  x      |  x                        |  x     |  x        |  x         |  3, RxD   |  1, MISO  |  x           |  0,1 WO5      |  3 WO    | WOB      |  x       |  x
    PORT_PIN_PB06 = 14U, // PB6 <--  x                |  x      |  x                        |  x     |  x        |  x         |  3, XCK   |  1, SCK   |  1, SDA(S)   |  x            |  x       | WOC      |  x       |  LUT4, OUT
    PORT_PIN_PB07 = 15U, // PB7 <--  x                |  x      |  x                        |  x     |  x        |  x         |  3, XDIR  |  1, !SS   |  1, SDA(S)   |  x            |  x       | WOD      |  EVOUTB  |  x

    PORT_PIN_PC00 = 16U, // PC0 <--  x                |  x      |  x                        |  x     |  x        |  x         |  1, TxD   |  1, MOSI  |  x           |  0 WO0        |  2 WO    | x        |  x       |  LUT1, IN0
    PORT_PIN_PC01 = 17U, // PC1 <--  x                |  x      |  x                        |  x     |  x        |  x         |  1, RxD   |  1, MISO  |  x           |  0 WO1        |  3 WO    | x        |  x       |  LUT1, IN1
    PORT_PIN_PC02 = 18U, // PC2 <--  TWI, Fm+         |  x      |  x                        |  x     |  x        |  x         |  1, XCK   |  1, SCK   |  0, SDA(MS)  |  0 WO2        |  x       | x        |  EVOUTC  |  LUT1, IN2
    PORT_PIN_PC03 = 19U, // PC3 <--  TWI, Fm+         |  x      |  x                        |  x     |  x        |  x         |  1, XDIR  |  1, !SS   |  0, SCL(MS)  |  0 WO3        |  x       | x        |  x       |  LUT1, OUT
    PORT_PIN_PC04 = 20U, // PC4 <--  x                |  x      |  x                        |  x     |  x        |  x         |  1, TxD   |  1, MOSI  |  x           |  0 WO4, 1 WO0 |  x       | x        |  x       |  x
    PORT_PIN_PC05 = 21U, // PC5 <--  x                |  x      |  x                        |  x     |  x        |  x         |  1, RxD   |  1, MISO  |  x           |  0 WO5, 1 WO1 |  x       | x        |  x       |  x
    PORT_PIN_PC06 = 22U, // PC6 <--  x                |  x      |  0,1,2 OUT                |  x     |  x        |  x         |  1, XCK   |  1, SCK   |  0, SDA(S)   |  1 WO2        |  x       | x        |  x       |  LUT1, OUT
    PORT_PIN_PC07 = 23U, // PC7 <--  x                |  x      |  x                        |  x     |  x        |  0,1,2 OUT |  1, XDIR  |  1, !SS   |  0, SCL(S)   |  x            |  x       | x        |  EVOUTC  |  x

    PORT_PIN_PD00 = 24U, // PD0 <--  x                |  AIN0   |  0,1,2 AINN1              |  x     |  x        |  x         |  x        |  x        |  x           |  0 WO0        |  x       | x        |  x       |  LUT2, IN0
    PORT_PIN_PD01 = 25U, // PD1 <--  x                |  AIN1   |  x                        |  x     |  OP0, INP |  0, ZCIN   |  x        |  x        |  x           |  0 WO1        |  x       | x        |  x       |  LUT2, IN1
    PORT_PIN_PD02 = 26U, // PD2 <--  x                |  AIN2   |  0,1,2 AINP0              |  x     |  OP0, OUT |  x         |  x        |  x        |  x           |  0 WO2        |  x       | x        |  EVOUTD  |  LUT2, IN2
    PORT_PIN_PD03 = 27U, // PD3 <--  x                |  AIN3   |  0 AINN0, 1 AINP0         |  x     |  OP0, INN |  x         |  x        |  x        |  x           |  0 WO3        |  x       | x        |  x       |  LUT2, OUT
    PORT_PIN_PD04 = 28U, // PD4 <--  x                |  AIN4   |  1 AINP2, 2 AINP1         |  x     |  OP1, INP |  x         |  x        |  x        |  x           |  0 WO4        |  x       | x        |  x       |  x
    PORT_PIN_PD05 = 29U, // PD5 <--  x                |  AIN5   |  1 AINN0                  |  x     |  OP1, OUT |  x         |  x        |  x        |  x           |  0 WO5        |  x       | x        |  x       |  x
    PORT_PIN_PD06 = 30U, // PD6 <--  x                |  AIN6   |  0,1,2 AINP3              |  OUT   |  x        |  x         |  x        |  x        |  x           |  x            |  x       | x        |  x       |  LUT2, OUT
    PORT_PIN_PD07 = 31U, // PD7 <--  VREFA            |  AIN7   |  0,1 AINN2, 2 AINN0/AINN2 |  x     |  OP1, INN |  x         |  x        |  x        |  x           |  x            |  x       | x        |  EVOUTD  |  x

    PORT_PIN_PE00 = 32U, // PE0 <--  x                |  AIN8   |  0 AINP1                  |  x     |  x        |  x         |  4, TxD   |  0, MOSI  |  x           |  0 WO0        |  x       | x        |  x       |  x
    PORT_PIN_PE01 = 33U, // PE1 <--  x                |  AIN9   |  2 AINP2                  |  x     |  OP2, INP |  x         |  4, RxD   |  0, MISO  |  x           |  0 WO1        |  x       | x        |  x       |  x
    PORT_PIN_PE02 = 34U, // PE2 <--  x                |  AIN10  |  0 AINP2                  |  x     |  OP2, OUT |  x         |  4, XCK   |  0, SCK   |  x           |  0 WO2        |  x       | x        |  EVOUTE  |  x
    PORT_PIN_PE03 = 35U, // PE3 <--  x                |  AIN11  |  x                        |  x     |  OP2, INN |  1, ZCIN   |  4, XDIR  |  0, !SS   |  x           |  0 WO3        |  x       | x        |  x       |  x
    PORT_PIN_PE04 = 36U, // PE4 <--  x                |  AIN12  |  x                        |  x     |  x        |  x         |  4, TxD   |  x        |  x           |  0 WO4, 1 WO0 |  x       | x        |  x       |  x
    PORT_PIN_PE05 = 37U, // PE5 <--  x                |  AIN13  |  x                        |  x     |  x        |  x         |  4, RxD   |  x        |  x           |  0 WO5, 1 WO1 |  x       | x        |  x       |  x
    PORT_PIN_PE06 = 38U, // PE6 <--  x                |  AIN14  |  x                        |  x     |  x        |  x         |  4, XCK   |  x        |  x           |  1 WO2        |  x       | x        |  x       |  x
    PORT_PIN_PE07 = 39U, // PE7 <--  x                |  AIN15  |  x                        |  x     |  x        |  2, ZCIN   |  4, XDIR  |  x        |  x           |  x            |  x       | x        |  EVOUTE  |  x

    PORT_PIN_PF00 = 40U, // PF0 <--  XTAL32K1         |  AIN16  |  x                        |  x     |  x        |  x         |  2, TxD   |  x        |  x           |  0 WO0        |  x       | WOA      |  x       |  LUT3, IN0
    PORT_PIN_PF01 = 41U, // PF1 <--  XTAL32K2         |  AIN17  |  x                        |  x     |  x        |  x         |  2, RxD   |  x        |  x           |  0 WO1        |  x       | WOB      |  x       |  LUT3, IN1
    PORT_PIN_PF02 = 42U, // PF2 <--  TWI, Fm+         |  AIN18  |  x                        |  x     |  x        |  x         |  2, XCK   |  x        |  1, SDA(MS)  |  0 WO2        |  x       | WOC      |  EVOUTF  |  LUT3, IN2
    PORT_PIN_PF03 = 43U, // PF3 <--  TWI, Fm+         |  AIN19  |  x                        |  x     |  x        |  x         |  2, XDIR  |  x        |  1, SCL(MS)  |  0 WO3        |  x       | WOD      |  x       |  LUT3, OUT
    PORT_PIN_PF04 = 44U, // PF4 <--  x                |  AIN20  |  x                        |  x     |  x        |  x         |  2, TxD   |  x        |  x           |  0 WO4        |  0 WO    | x        |  x       |  x
    PORT_PIN_PF05 = 45U, // PF5 <--  x                |  AIN21  |  x                        |  x     |  x        |  x         |  2, RxD   |  x        |  x           |  0 WO5        |  1 WO    | x        |  x       |  x
    PORT_PIN_PF06 = 46U, // PF6 <--  !RESET           |  x      |  x                        |  x     |  x        |  x         |  x        |  x        |  x           |  x            |  x       | x        |  x       |  x
    PORT_PIN_PF07 = 47U, // PF7 <--  UPDI             |  x      |  x                        |  x     |  x        |  x         |  x        |  x        |  x           |  x            |  x       | x        |  x       |  x

    PORT_PIN_NONE = 65535U
} PORT_PIN_e;



void PORT_PinOutputEnable(PORT_PIN_e pin);
void PORT_PinInputEnable(PORT_PIN_e pin);
void PORT_PinInputPullupEnable(PORT_PIN_e pin);
void PORT_PinWrite(PORT_PIN_e pin, bool value);
void PORT_PinToggle(PORT_PIN_e pin);
bool PORT_PinRead(PORT_PIN_e pin);


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

}

#endif
// DOM-IGNORE-END

#endif // PORT_H
