/*******************************************************************************
  PLIB PORT Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    blib_port.c

  Summary:
    PLIB PORT library Source File
 
  Version:
    1.1 stable

  Description:
    This file provides basic functions for PORT periphery.

*******************************************************************************/


#include "port.h"



// *****************************************************************************
/* Function:
    void PORT_PinOutputEnable(PORT_PIN_e pin)

  Summary:
    Enables selected IO pin as output.

  Description:
    This function enables selected IO pin as output.

  Parameters:
    pin - One of the IO pins from the enum PORT_PIN_e.

  Returns:
    None.

  Example:
    <code>

    PORT_PinOutputEnable(USER_LED_PIN);

    </code>

  Remarks:
    None.
*/

void PORT_PinOutputEnable(PORT_PIN_e pin)
{
    uint8_t shift = pin & 0b111;
    switch(pin >> 3){
        case PORT_PORTA:
            PORTA.DIR = PORTA.DIR | (1 << shift);
        break;

        case PORT_PORTB:
            PORTB.DIR = PORTB.DIR | (1 << shift);
        break;

        case PORT_PORTC:
            PORTC.DIR = PORTC.DIR | (1 << shift);
        break;

        case PORT_PORTD:
            PORTD.DIR = PORTD.DIR | (1 << shift);
        break;

        case PORT_PORTE:
            PORTE.DIR = PORTE.DIR | (1 << shift);
        break;

        case PORT_PORTF:
            PORTF.DIR = PORTF.DIR | (1 << shift);
        break;

        default:
            // Nothing
        break;
    }
    
}



// *****************************************************************************
/* Function:
    void PORT_PinInputEnable(PORT_PIN_e pin)

  Summary:
    Enables selected IO pin as input.

  Description:
    This function enables selected IO pin as input.

  Parameters:
    pin - One of the IO pins from the enum PORT_PIN_e.

  Returns:
    None.

  Example:
    <code>

    PORT_PinInputEnable(USER_BUTTON_PIN);

    </code>

  Remarks:
    None.
*/

void PORT_PinInputEnable(PORT_PIN_e pin)
{
    uint8_t shift = pin & 0b111;
    switch(pin >> 3){
        case PORT_PORTA:
            PORTA.DIR = PORTA.DIR & ~(1 << shift);
        break;

        case PORT_PORTB:
            PORTB.DIR = PORTB.DIR & ~(1 << shift);
        break;

        case PORT_PORTC:
            PORTC.DIR = PORTC.DIR & ~(1 << shift);
        break;

        case PORT_PORTD:
            PORTD.DIR = PORTD.DIR & ~(1 << shift);
        break;

        case PORT_PORTE:
            PORTE.DIR = PORTE.DIR & ~(1 << shift);
        break;

        case PORT_PORTF:
            PORTF.DIR = PORTF.DIR & ~(1 << shift);
        break;

        default:
            // Nothing
        break;
    }
    
}



// *****************************************************************************
/* Function:
    void PORT_PinInputPullupEnable(PORT_PIN_e pin)

  Summary:
    Enables selected IO pin as input.

  Description:
    This function enables selected IO pin as input.

  Parameters:
    pin - One of the IO pins from the enum PORT_PIN_e.

  Returns:
    None.

  Example:
    <code>

    PORT_PinInputPullupEnable(USER_BUTTON_PIN);

    </code>

  Remarks:
    None.
*/

void PORT_PinInputPullupEnable(PORT_PIN_e pin)
{
    uint8_t shift = pin & 0b111;
    switch(pin >> 3){
        case PORT_PORTA:
            PORTA.DIR = PORTA.DIR & ~(1 << shift);
            switch(pin & 0b111){
                case 0:
                    PORTA.PIN0CTRL = PORTA.PIN0CTRL | 0b00001000; // PORT_PULLUPEN_bm
                break;
                
                case 1:
                    PORTA.PIN1CTRL = PORTA.PIN1CTRL | 0b00001000;
                break;
                
                case 2:
                    PORTA.PIN2CTRL = PORTA.PIN2CTRL | 0b00001000;
                break;
                
                case 3:
                    PORTA.PIN3CTRL = PORTA.PIN3CTRL | 0b00001000;
                break;
                
                case 4:
                    PORTA.PIN4CTRL = PORTA.PIN4CTRL | 0b00001000;
                break;
                
                case 5:
                    PORTA.PIN5CTRL = PORTA.PIN5CTRL | 0b00001000;
                break;
                
                case 6:
                    PORTA.PIN6CTRL = PORTA.PIN6CTRL | 0b00001000;
                break;
                
                case 7:
                    PORTA.PIN7CTRL = PORTA.PIN7CTRL | 0b00001000;
                break;
            }
        break;

        case PORT_PORTB:
            PORTB.DIR = PORTB.DIR & ~(1 << shift);
            switch(pin & 0b111){
                case 0:
                    PORTB.PIN0CTRL = PORTB.PIN0CTRL | 0b00001000;
                break;
                
                case 1:
                    PORTB.PIN1CTRL = PORTB.PIN1CTRL | 0b00001000;
                break;
                
                case 2:
                    PORTB.PIN2CTRL = PORTB.PIN2CTRL | 0b00001000;
                break;
                
                case 3:
                    PORTB.PIN3CTRL = PORTB.PIN3CTRL | 0b00001000;
                break;
                
                case 4:
                    PORTB.PIN4CTRL = PORTB.PIN4CTRL | 0b00001000;
                break;
                
                case 5:
                    PORTB.PIN5CTRL = PORTB.PIN5CTRL | 0b00001000;
                break;
                
                case 6:
                    PORTB.PIN6CTRL = PORTB.PIN6CTRL | 0b00001000;
                break;
                
                case 7:
                    PORTB.PIN7CTRL = PORTB.PIN7CTRL | 0b00001000;
                break;
            }
        break;

        case PORT_PORTC:
            PORTC.DIR = PORTC.DIR & ~(1 << shift);
            switch(pin & 0b111){
                case 0:
                    PORTC.PIN0CTRL = PORTC.PIN0CTRL | 0b00001000;
                break;
                
                case 1:
                    PORTC.PIN1CTRL = PORTC.PIN1CTRL | 0b00001000;
                break;
                
                case 2:
                    PORTC.PIN2CTRL = PORTC.PIN2CTRL | 0b00001000;
                break;
                
                case 3:
                    PORTC.PIN3CTRL = PORTC.PIN3CTRL | 0b00001000;
                break;
                
                case 4:
                    PORTC.PIN4CTRL = PORTC.PIN4CTRL | 0b00001000;
                break;
                
                case 5:
                    PORTC.PIN5CTRL = PORTC.PIN5CTRL | 0b00001000;
                break;
                
                case 6:
                    PORTC.PIN6CTRL = PORTC.PIN6CTRL | 0b00001000;
                break;
                
                case 7:
                    PORTC.PIN7CTRL = PORTC.PIN7CTRL | 0b00001000;
                break;
            }
        break;

        case PORT_PORTD:
            PORTD.DIR = PORTD.DIR & ~(1 << shift);
            switch(pin & 0b111){
                case 0:
                    PORTD.PIN0CTRL = PORTD.PIN0CTRL | 0b00001000;
                break;
                
                case 1:
                    PORTD.PIN1CTRL = PORTD.PIN1CTRL | 0b00001000;
                break;
                
                case 2:
                    PORTD.PIN2CTRL = PORTD.PIN2CTRL | 0b00001000;
                break;
                
                case 3:
                    PORTD.PIN3CTRL = PORTD.PIN3CTRL | 0b00001000;
                break;
                
                case 4:
                    PORTD.PIN4CTRL = PORTD.PIN4CTRL | 0b00001000;
                break;
                
                case 5:
                    PORTD.PIN5CTRL = PORTD.PIN5CTRL | 0b00001000;
                break;
                
                case 6:
                    PORTD.PIN6CTRL = PORTD.PIN6CTRL | 0b00001000;
                break;
                
                case 7:
                    PORTD.PIN7CTRL = PORTD.PIN7CTRL | 0b00001000;
                break;
            }
        break;

        case PORT_PORTE:
            PORTE.DIR = PORTE.DIR & ~(1 << shift);
            switch(pin & 0b111){
                case 0:
                    PORTE.PIN0CTRL = PORTE.PIN0CTRL | 0b00001000;
                break;
                
                case 1:
                    PORTE.PIN1CTRL = PORTE.PIN1CTRL | 0b00001000;
                break;
                
                case 2:
                    PORTE.PIN2CTRL = PORTE.PIN2CTRL | 0b00001000;
                break;
                
                case 3:
                    PORTE.PIN3CTRL = PORTE.PIN3CTRL | 0b00001000;
                break;
                
                case 4:
                    PORTE.PIN4CTRL = PORTE.PIN4CTRL | 0b00001000;
                break;
                
                case 5:
                    PORTE.PIN5CTRL = PORTE.PIN5CTRL | 0b00001000;
                break;
                
                case 6:
                    PORTE.PIN6CTRL = PORTE.PIN6CTRL | 0b00001000;
                break;
                
                case 7:
                    PORTE.PIN7CTRL = PORTE.PIN7CTRL | 0b00001000;
                break;
            }
        break;

        case PORT_PORTF:
            PORTF.DIR = PORTF.DIR & ~(1 << shift);
            switch(pin & 0b111){
                case 0:
                    PORTF.PIN0CTRL = PORTF.PIN0CTRL | 0b00001000;
                break;
                
                case 1:
                    PORTF.PIN1CTRL = PORTF.PIN1CTRL | 0b00001000;
                break;
                
                case 2:
                    PORTF.PIN2CTRL = PORTF.PIN2CTRL | 0b00001000;
                break;
                
                case 3:
                    PORTF.PIN3CTRL = PORTF.PIN3CTRL | 0b00001000;
                break;
                
                case 4:
                    PORTF.PIN4CTRL = PORTF.PIN4CTRL | 0b00001000;
                break;
                
                case 5:
                    PORTF.PIN5CTRL = PORTF.PIN5CTRL | 0b00001000;
                break;
                
                case 6:
                    PORTF.PIN6CTRL = PORTF.PIN6CTRL | 0b00001000;
                break;
                
                case 7:
                    PORTF.PIN7CTRL = PORTF.PIN7CTRL | 0b00001000;
                break;
            }
        break;

        default:
            // Nothing
        break;
    }   
}



// *****************************************************************************
/* Function:
    void PORT_PinWrite(PORT_PIN_e pin, bool value)

  Summary:
    Write logical value to selected IO pin.

  Description:
    This function write logical voltage level to selected IO pin.

  Parameters:
    pin - One of the IO pins from the enum PORT_PIN_e.
    value - Boolean logical value.

  Returns:
    None.

  Example:
    <code>

    PORT_PinWrite(USER_LED_PIN, LOW);

    </code>

  Remarks:
    None.
*/

void PORT_PinWrite(PORT_PIN_e pin, bool value)
{
    uint8_t shift = pin & 0b111;
    switch(pin >> 3){
        case PORT_PORTA:
            PORTA.OUT = (value) ? PORTA.OUT | (1 << shift) : PORTA.OUT & ~(1 << shift);
        break;

        case PORT_PORTB:
            PORTB.OUT = (value) ? PORTB.OUT | (1 << shift) : PORTB.OUT & ~(1 << shift);
        break;

        case PORT_PORTC:
            PORTC.OUT = (value) ? PORTC.OUT | (1 << shift) : PORTC.OUT & ~(1 << shift);
        break;

        case PORT_PORTD:
            PORTD.OUT = (value) ? PORTD.OUT | (1 << shift) : PORTD.OUT & ~(1 << shift);
        break;

        case PORT_PORTE:
            PORTE.OUT = (value) ? PORTE.OUT | (1 << shift) : PORTE.OUT & ~(1 << shift);
        break;

        case PORT_PORTF:
            PORTF.OUT = (value) ? PORTF.OUT | (1 << shift) : PORTF.OUT & ~(1 << shift);
        break;

        default:
            // Nothing
        break;
    }
}



// *****************************************************************************
/* Function:
    void PORT_PinToggle(PORT_PIN_e pin)

  Summary:
    Toggle logical value on selected IO pin.

  Description:
    This function toggle logical voltage level on selected IO pin.

  Parameters:
    pin - One of the IO pins from the enum PORT_PIN_e.

  Returns:
    None.

  Example:
    <code>

    PORT_PinToggle(USER_LED_PIN);

    </code>

  Remarks:
    None.
*/

void PORT_PinToggle(PORT_PIN_e pin)
{
    uint8_t shift = pin & 0b111;
    switch(pin >> 3){
        case PORT_PORTA:
            PORTA.OUTTGL = 1 << shift;
        break;

        case PORT_PORTB:
            PORTB.OUTTGL = 1 << shift;
        break;

        case PORT_PORTC:
            PORTC.OUTTGL = 1 << shift;
        break;

        case PORT_PORTD:
            PORTD.OUTTGL = 1 << shift;
        break;

        case PORT_PORTE:
            PORTE.OUTTGL = 1 << shift;
        break;

        case PORT_PORTF:
            PORTF.OUTTGL = 1 << shift;
        break;

        default:
            // Nothing
        break;
    }
}



// *****************************************************************************
/* Function:
    bool PORT_PinRead(PORT_PIN_e pin)

  Summary:
    Read logical value at selected IO pin.

  Description:
    This function reads boolean voltage logical level at the selected input IO pin.

  Parameters:
    pin - One of the IO pins from the enum PORT_PIN_e.

  Returns:
    true - the logical level at the pin is a logic high.
    false - the logical level at the pin is a logic low.

  Example:
    <code>

    bool value;
    value = PORT_PinRead(USER_BUTTON_PIN);

    </code>

  Remarks:
    None.
*/

bool PORT_PinRead(PORT_PIN_e pin)
{
    bool value;
    uint8_t shift = pin & 0b111;
    switch(pin >> 3){
        case PORT_PORTA:
            value = (PORTA.IN >> shift) & 1;
        break;

        case PORT_PORTB:
            value = (PORTB.IN >> shift) & 1;
        break;

        case PORT_PORTC:
            value = (PORTC.IN >> shift) & 1;
        break;

        case PORT_PORTD:
            value = (PORTD.IN >> shift) & 1;
        break;

        case PORT_PORTE:
            value = (PORTE.IN >> shift) & 1;
        break;

        case PORT_PORTF:
            value = (PORTF.IN >> shift) & 1;
        break;

        default:
            value = 0;
        break;
    }
    return value;
}
