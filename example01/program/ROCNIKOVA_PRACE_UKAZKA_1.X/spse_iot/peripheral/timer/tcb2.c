/*******************************************************************************
  Timer TCB2 Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    tcb2.c

  Summary:
    Timer TCB2 library Source File
 
  Version:
    1.0

  Description:
    This file provides basic functions for timer TCB2 periphery.

*******************************************************************************/

#include "tcb2.h"
#include <stdlib.h>
#include <avr/interrupt.h>

volatile uint64_t _millis = 0;

ISR(TCB2_INT_vect)
{
    if(TCB2.INTFLAGS & TCB_CAPT_bm){
        _millis++;
        TCB2.CNT = 0;
        TCB2.INTFLAGS = TCB_CAPT_bm;
    } 
}

void TCB2_Initialize(void)
{
    //Compare or Capture
    TCB2.CCMP = 0x7D0;

    //Count
    TCB2.CNT = 0x0;

    //ASYNC enabled; CCMPEN disabled; CCMPINIT disabled; CNTMODE INT; 
    TCB2.CTRLB = 0x40;
    
    //DBGRUN disabled; 
    TCB2.DBGCTRL = 0x0;

    //CAPTEI disabled; EDGE disabled; FILTER disabled; 
    TCB2.EVCTRL = 0x0;

    //CAPT enabled; OVF disabled; 
    TCB2.INTCTRL = TCB_CAPT_bm;

    //CAPT disabled; OVF disabled; 
    TCB2.INTFLAGS = 0x0;

    //Temporary Value
    TCB2.TEMP = 0x0;

    //CASCADE disabled; CLKSEL DIV2; ENABLE enabled; RUNSTDBY enabled; SYNCUPD disabled; 
    TCB2.CTRLA = TCB_CLKSEL_0_bm | TCB_ENABLE_bm | TCB_RUNSTDBY_bm;

}

uint64_t TCB2_GetMillis(void){
    return _millis;
}