/*******************************************************************************
  Timer TCB2 Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    tcb2.h

  Summary:
    Timer TCB2 library Header File
 
  Version:
    1.0

  Description:
    This file provides basic functions for timer TCB2 periphery.

*******************************************************************************/

#ifndef TCB2_H
#define TCB2_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

    void TCB2_Initialize(void);
    uint64_t TCB2_GetMillis(void);


#ifdef __cplusplus
}
#endif

#endif /* TCB2_H_INCLUDED */