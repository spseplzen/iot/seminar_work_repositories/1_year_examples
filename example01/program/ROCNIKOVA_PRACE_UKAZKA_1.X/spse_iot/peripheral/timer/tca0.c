/*******************************************************************************
  Timer TCA0 Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    tca0.c

  Summary:
    Timer TCA0 library Source File
 
  Version:
    1.1

  Description:
    This file provides basic functions for timer TCA0 periphery.

*******************************************************************************/


#include "tca0.h"
#include <stdlib.h>
#include <avr/interrupt.h>

static tca0_t _intern_tca0 = {};

void TCA0_DefaultCompare0CallbackRegister(void);
void (*TCA0_CMP0_isr_cb)(void) = &TCA0_DefaultCompare0CallbackRegister;
void TCA0_DefaultCompare1CallbackRegister(void);
void (*TCA0_CMP1_isr_cb)(void) = &TCA0_DefaultCompare1CallbackRegister;
void TCA0_DefaultCompare2CallbackRegister(void);
void (*TCA0_CMP2_isr_cb)(void) = &TCA0_DefaultCompare2CallbackRegister;
void TCA0_DefaultOverflowCallbackRegister(void);
void (*TCA0_OVF_isr_cb)(void) = &TCA0_DefaultOverflowCallbackRegister;

void TCA0_DefaultCompare0CallbackRegister(void)
{
    //Add your ISR code here
}

void TCA0_DefaultCompare1CallbackRegister(void)
{
    //Add your ISR code here
}

void TCA0_DefaultCompare2CallbackRegister(void)
{
    //Add your ISR code here
}

void TCA0_DefaultOverflowCallbackRegister(void)
{
    //Add your ISR code here
}


ISR(TCA0_CMP0_vect)
{
    if (TCA0_CMP0_isr_cb != NULL)
        (*TCA0_CMP0_isr_cb)();
    
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_CMP0_bm;
}

ISR(TCA0_CMP1_vect)
{
    if (TCA0_CMP1_isr_cb != NULL)
        (*TCA0_CMP1_isr_cb)();
    
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_CMP1_bm;
}

ISR(TCA0_CMP2_vect)
{
    if (TCA0_CMP2_isr_cb != NULL)
        (*TCA0_CMP2_isr_cb)();
    
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_CMP2_bm;
}

ISR(TCA0_OVF_vect)
{
    if (TCA0_OVF_isr_cb != NULL)
        (*TCA0_OVF_isr_cb)();
    
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_OVF_bm;
}


void TCA0_Initialize(tca0_config_t *config, uint32_t clk){
    _intern_tca0.config.clksel     = config->clksel;
    _intern_tca0.config.period     = config->period;
    _intern_tca0.config.cmpen.cmp0 = config->cmpen.cmp0;
    _intern_tca0.config.cmpen.cmp1 = config->cmpen.cmp1;
    _intern_tca0.config.cmpen.cmp2 = config->cmpen.cmp2;
    _intern_tca0.config.wgmode     = config->wgmode;
    _intern_tca0.config.enable     = config->enable;
    _intern_tca0.config.out.port   = config->out.port;
    _intern_tca0.config.out.enable = config->out.enable;
    
    if(_intern_tca0.config.out.enable == true){
        if(_intern_tca0.config.out.port == PORT_PORTA) PORTMUX.TCAROUTEA = PORTMUX_TCA0_PORTA_gc;      /* PA0, PA1, PA2, PA3, PA4, PA5 */
        else if(_intern_tca0.config.out.port == PORT_PORTB) PORTMUX.TCAROUTEA = PORTMUX_TCA0_PORTB_gc; /* PB0, PB1, PB2, PB3, PB4, PB5 */
        else if(_intern_tca0.config.out.port == PORT_PORTC) PORTMUX.TCAROUTEA = PORTMUX_TCA0_PORTC_gc; /* PC0, PC1, PC2, PC3, PC4, PC5 */
        else if(_intern_tca0.config.out.port == PORT_PORTD) PORTMUX.TCAROUTEA = PORTMUX_TCA0_PORTD_gc; /* PD0, PD1, PD2, PD3, PD4, PD5 */
        else if(_intern_tca0.config.out.port == PORT_PORTE) PORTMUX.TCAROUTEA = PORTMUX_TCA0_PORTE_gc; /* PE0, PE1, PE2, PE3, PE4, PE5 */
        else if(_intern_tca0.config.out.port == PORT_PORTF) PORTMUX.TCAROUTEA = PORTMUX_TCA0_PORTF_gc; /* PF0, PF1, PF2, PF3, PF4, PF5 */
    }
    
    if(_intern_tca0.config.wgmode == TCA0_WGMODE_NORMAL) TCA0.SINGLE.CTRLB = TCA_SINGLE_WGMODE_NORMAL_gc;
    else if(_intern_tca0.config.wgmode == TCA0_WGMODE_FREQUENCY) TCA0.SINGLE.CTRLB = TCA_SINGLE_WGMODE_FRQ_gc;
    else if(_intern_tca0.config.wgmode == TCA0_WGMODE_SINGLESLOPE_PWM) TCA0.SINGLE.CTRLB = TCA_SINGLE_WGMODE_SINGLESLOPE_gc;
    else if(_intern_tca0.config.wgmode == TCA0_WGMODE_DSTOP_PWM) TCA0.SINGLE.CTRLB = TCA_SINGLE_WGMODE_DSTOP_gc;
    else if(_intern_tca0.config.wgmode == TCA0_WGMODE_DSBOTH_PWM) TCA0.SINGLE.CTRLB = TCA_SINGLE_WGMODE_DSBOTH_gc;
    else if(_intern_tca0.config.wgmode == TCA0_WGMODE_DSBOTTOM_PWM) TCA0.SINGLE.CTRLB = TCA_SINGLE_WGMODE_DSBOTTOM_gc;
    
    if(_intern_tca0.config.cmpen.cmp0 == true){
        TCA0.SINGLE.CTRLB |= TCA_SINGLE_CMP0EN_bm;
        _intern_tca0.config.cmpval.cmp0 = config->cmpval.cmp0;
    }
    else{
        _intern_tca0.config.cmpval.cmp0 = 0;
    }
    
    if(_intern_tca0.config.cmpen.cmp1 == true){
        TCA0.SINGLE.CTRLB |= TCA_SINGLE_CMP1EN_bm;
        _intern_tca0.config.cmpval.cmp1 = config->cmpval.cmp1;
    }
    else{
        _intern_tca0.config.cmpval.cmp1 = 0;
    }
    
    if(_intern_tca0.config.cmpen.cmp2 == true){
        TCA0.SINGLE.CTRLB |= TCA_SINGLE_CMP2EN_bm;
        _intern_tca0.config.cmpval.cmp2 = config->cmpval.cmp2;
    }
    else{
        _intern_tca0.config.cmpval.cmp2 = 0;
    }
    
    TCA0.SINGLE.PER = _intern_tca0.config.period;
    
    TCA0.SINGLE.CMP0  = _intern_tca0.config.cmpval.cmp0;
    TCA0.SINGLE.CMP1  = _intern_tca0.config.cmpval.cmp1;
    TCA0.SINGLE.CMP1  = _intern_tca0.config.cmpval.cmp2;
    
    TCA0.SINGLE.INTFLAGS = 0x0;
    
    // SPLITM disabled; 
    TCA0.SINGLE.CTRLD = 0x0;
    
    // CMP0OV disabled; CMP1OV disabled; CMP2OV disabled; 
    TCA0.SINGLE.CTRLC = 0x0;
    
    // Count
    TCA0.SINGLE.CNT = 0x0;
    
    if(_intern_tca0.config.clksel == TCA0_CLKSEL_DIV1) TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV1_gc;
    else if(_intern_tca0.config.clksel == TCA0_CLKSEL_DIV2) TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV2_gc;
    else if(_intern_tca0.config.clksel == TCA0_CLKSEL_DIV4) TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV4_gc;
    else if(_intern_tca0.config.clksel == TCA0_CLKSEL_DIV8) TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV8_gc;
    else if(_intern_tca0.config.clksel == TCA0_CLKSEL_DIV16) TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV16_gc;
    else if(_intern_tca0.config.clksel == TCA0_CLKSEL_DIV64) TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV64_gc;
    else if(_intern_tca0.config.clksel == TCA0_CLKSEL_DIV256) TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV256_gc;
    else if(_intern_tca0.config.clksel == TCA0_CLKSEL_DIV1024) TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV1024_gc;
    
    if(_intern_tca0.config.enable == true) TCA0.SINGLE.CTRLA |= TCA_SINGLE_ENABLE_bm;
}

void TCA0_Start(void){
    TCA0.SINGLE.CTRLA |= TCA_SINGLE_ENABLE_bm;
}

void TCA0_Stop(void){
    TCA0.SINGLE.CTRLA &= ~TCA_SINGLE_ENABLE_bm;
}

void TCA0_Write(uint16_t val){
    TCA0.SINGLE.PER = val;
}

uint16_t TCA0_Read(){
    return TCA0.SINGLE.CNT;
}

void TCA0_Compare0MatchSet(uint16_t cmp){
    //TCA0.SINGLE.CMP0 = cmp;
    TCA0.SINGLE.CMP0L = cmp & 0xFF;
    TCA0.SINGLE.CMP0H = (cmp >> 8) & 0xFF;
}

void TCA0_Compare1MatchSet(uint16_t cmp){
    //TCA0.SINGLE.CMP0 = cmp;
    TCA0.SINGLE.CMP1L = cmp & 0xFF;
    TCA0.SINGLE.CMP1H = (cmp >> 8) & 0xFF;
}

void TCA0_Compare2MatchSet(uint16_t cmp){
    //TCA0.SINGLE.CMP0 = cmp;
    TCA0.SINGLE.CMP2L = cmp & 0xFF;
    TCA0.SINGLE.CMP2H = (cmp >> 8) & 0xFF;
}

void TCA0_EnableAllInterrupt(){
    TCA0.SINGLE.INTCTRL = TCA_SINGLE_CMP0_bm | TCA_SINGLE_CMP1_bm | TCA_SINGLE_CMP2_bm | TCA_SINGLE_OVF_bm;
}

void TCA0_DisableAllInterrupt(){
    TCA0.SINGLE.INTCTRL = 0b00000000;
}

void TCA0_EnableOVFInterrupt(){
    TCA0.SINGLE.INTCTRL |= TCA_SINGLE_OVF_bm;
}

void TCA0_DisableOVFInterrupt(){
    TCA0.SINGLE.INTCTRL &= ~TCA_SINGLE_OVF_bm;
}

void TCA0_EnableCMP0Interrupt(){
    TCA0.SINGLE.INTCTRL |= TCA_SINGLE_CMP0_bm;
}

void TCA0_DisableCMP0Interrupt(){
    TCA0.SINGLE.INTCTRL &= ~TCA_SINGLE_CMP0_bm;
}

void TCA0_EnableCMP1Interrupt(){
    TCA0.SINGLE.INTCTRL |= TCA_SINGLE_CMP1_bm;
}

void TCA0_DisableCMP1Interrupt(){
    TCA0.SINGLE.INTCTRL &= ~TCA_SINGLE_CMP1_bm;
}

void TCA0_EnableCMP2Interrupt(){
    TCA0.SINGLE.INTCTRL |= TCA_SINGLE_CMP2_bm;
}

void TCA0_DisableCMP2Interrupt(){
    TCA0.SINGLE.INTCTRL &= ~TCA_SINGLE_CMP2_bm;
}

void TCA0_ClearOverflowInterruptFlag(){
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_OVF_bm;
}
bool TCA0_IsOverflowInterruptFlagSet(){
    return (TCA0.SINGLE.INTFLAGS & TCA_SINGLE_OVF_bm);
}

void TCA0_ClearCMP0InterruptFlag(){
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_CMP0_bm;
}

bool TCA0_IsCMP0InterruptFlagSet(){
    return ((TCA0.SINGLE.INTFLAGS & TCA_SINGLE_CMP0_bm) >> TCA_SINGLE_CMP0_bp);
}

void TCA0_ClearCMP1InterruptFlag(){
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_CMP1_bm;
}

bool TCA0_IsCMP1InterruptFlagSet(){
    return ((TCA0.SINGLE.INTFLAGS & TCA_SINGLE_CMP1_bm) >> TCA_SINGLE_CMP1_bp);
}

void TCA0_ClearCMP2InterruptFlag(){
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_CMP2_bm;
}

bool TCA0_IsCMP2InterruptFlagSet(){
    return ((TCA0.SINGLE.INTFLAGS & TCA_SINGLE_CMP2_bm) >> TCA_SINGLE_CMP2_bp);
}

void TCA0_OverflowCallbackRegister(tca0_callback_funcptr cb)
{
    TCA0_OVF_isr_cb = cb;
}

void TCA0_Compare0CallbackRegister(tca0_callback_funcptr cb)
{
    TCA0_CMP0_isr_cb = cb;
}

void TCA0_Compare1CallbackRegister(tca0_callback_funcptr cb)
{
    TCA0_CMP1_isr_cb = cb;
}

void TCA0_Compare2CallbackRegister(tca0_callback_funcptr cb)
{
    TCA0_CMP2_isr_cb = cb;
}