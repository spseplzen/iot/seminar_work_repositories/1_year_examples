/*******************************************************************************
  Timer TCA0 Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    tca0.h

  Summary:
    Timer TCA0 library Header File
 
  Version:
    1.1

  Description:
    This file provides basic functions for timer TCA0 periphery.

*******************************************************************************/


#ifndef TCA0_H
#define TCA0_H

#include "../plib_port/port.h"
#include <stdbool.h>
#include <stdint.h>


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
    extern "C" {
#endif
// DOM-IGNORE-END




// *****************************************************************************
// Section: Data types and constants
// *****************************************************************************

/*** Macros for boolean voltage logical level ***/

        
typedef void (*tca0_callback_funcptr)(void);

typedef struct tca0_descriptor_t tca0_t;
typedef struct tca0_callback_descriptor_t tca0_callback_t;
typedef struct tca0_config_descriptor_t tca0_config_t;
typedef struct tca0_cmpen_descriptor_t tca0_cmpen_t;
typedef struct tca0_cmpval_descriptor_t tca0_cmpval_t;
typedef struct tca0_out_descriptor_t tca0_out_t;

typedef enum{
    TCA0_CLKSEL_DIV1    = 0,
    TCA0_CLKSEL_DIV2    = 1,
    TCA0_CLKSEL_DIV4    = 2,
    TCA0_CLKSEL_DIV8    = 3,
    TCA0_CLKSEL_DIV16   = 4,
    TCA0_CLKSEL_DIV64   = 5,
    TCA0_CLKSEL_DIV256  = 6,
    TCA0_CLKSEL_DIV1024 = 7
} TCA0_CLKSEL_e;


typedef enum{
    TCA0_WGMODE_NORMAL          = 0,
    TCA0_WGMODE_FREQUENCY       = 1,
    TCA0_WGMODE_SINGLESLOPE_PWM = 3,
    TCA0_WGMODE_DSTOP_PWM       = 5,
    TCA0_WGMODE_DSBOTH_PWM      = 6,
    TCA0_WGMODE_DSBOTTOM_PWM    = 7
} TCA0_WGMODE_e;


struct tca0_callback_descriptor_t{
    tca0_callback_funcptr overflow_funcptr;
    tca0_callback_funcptr compare0_funcptr;
    tca0_callback_funcptr compare1_funcptr;
    tca0_callback_funcptr compare2_funcptr;
};

struct tca0_cmpen_descriptor_t{
    bool cmp0;
    bool cmp1;
    bool cmp2;
};

struct tca0_cmpval_descriptor_t{
    uint16_t cmp0;
    uint16_t cmp1;
    uint16_t cmp2;
};

struct tca0_out_descriptor_t{
    PORT_e port;
    bool enable;
};

struct tca0_config_descriptor_t{
    TCA0_CLKSEL_e clksel;
    uint16_t period;
    tca0_cmpen_t cmpen;
    tca0_cmpval_t cmpval;
    TCA0_WGMODE_e wgmode;
    tca0_out_t out;
    bool enable;
};

struct tca0_descriptor_t{
    tca0_config_t config;
};


void TCA0_Initialize(tca0_config_t *config, uint32_t clk);

void TCA0_Start();
void TCA0_Stop();

void TCA0_OverflowCallbackRegister(tca0_callback_funcptr cb);
void TCA0_Compare0CallbackRegister(tca0_callback_funcptr cb);
void TCA0_Compare1CallbackRegister(tca0_callback_funcptr cb);
void TCA0_Compare2CallbackRegister(tca0_callback_funcptr cb);

void TCA0_EnableAllInterrupt();
void TCA0_DisableAllInterrupt();
void TCA0_EnableOVFInterrupt();
void TCA0_DisableOVFInterrupt();
void TCA0_EnableCMP0Interrupt();
void TCA0_DisableCMP0Interrupt();
void TCA0_EnableCMP1Interrupt();
void TCA0_DisableCMP1Interrupt();
void TCA0_EnableCMP2Interrupt();
void TCA0_DisableCMP2Interrupt();

uint16_t TCA0_Read();

void TCA0_Write(uint16_t timerVal);

void TCA0_Compare0MatchSet(uint16_t cmp);
void TCA0_Compare1MatchSet(uint16_t cmp);
void TCA0_Compare2MatchSet(uint16_t cmp);

void TCA0_ClearOverflowInterruptFlag();
bool TCA0_IsOverflowInterruptFlagSet();

void TCA0_ClearCMP0InterruptFlag();
bool TCA0_IsCMP0InterruptFlagSet();

void TCA0_ClearCMP1InterruptFlag();
bool TCA0_IsCMP1InterruptFlagSet();

void TCA0_ClearCMP2InterruptFlag();
bool TCA0_IsCMP2InterruptFlagSet();



// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

}

#endif
// DOM-IGNORE-END

#endif // TCA0_H
