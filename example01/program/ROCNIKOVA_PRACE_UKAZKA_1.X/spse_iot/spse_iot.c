/*******************************************************************************
  SPSE IOT Library

  Company:
    VOS a SPSE Plzen
 
  Created by:
    Miroslav Soukup

  File Name:
    spse_iot.c

  Summary:
    SPSE IoT library Source File
 
  Version:
    1.2

  Description:
    This file provides basic functions.

*******************************************************************************/


#include "spse_iot.h"

void SPSE_IOT_Initialize(void){

    USER_LED_OutputEnable();
    USER_LED_Set();
    
    TCB2_Initialize();
    
    SPSE_IOT_GlobalInterruptsEnable();
    
}