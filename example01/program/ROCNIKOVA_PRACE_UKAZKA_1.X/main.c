/*
 * File:   main.c
 * Author: Jan Novak
 *
 * Created on March 22, 2023, 3:19 PM
 */


#include <xc.h> // Include the needed library

#include "spse_iot/spse_iot.h"

#include "spse_iot/easy_delay/easy_delay.h"
#include "spse_iot/peripheral/plib_port/port.h"
#include "spse_iot/peripheral/timer/tcb2.h"
#include "spse_iot/peripheral/sercom/usart/plib_sercom3_usart.h"

#include <stdio.h>

#define RADIATION_SOUND_ENABLE 600

#define SEGMENT_A PORT_PIN_PC00
#define SEGMENT_B PORT_PIN_PC01
#define SEGMENT_C PORT_PIN_PC02
#define SEGMENT_D PORT_PIN_PC03
#define SEGMENT_E PORT_PIN_PC04
#define SEGMENT_F PORT_PIN_PC05
#define SEGMENT_G PORT_PIN_PC06
#define SEGMENT_H PORT_PIN_PC07
#define DIGIT_0 PORT_PIN_PE00
#define DIGIT_1 PORT_PIN_PE01

#define GCIN PORT_PIN_PF04

#define PLED1 PORT_PIN_PF05


typedef enum{
    PRINT_NUMBER_SET_DIGIT0 = 0,
    PRINT_NUMBER_CLR_DIGIT0 = 1,
    PRINT_NUMBER_SET_DIGIT1 = 2,
    PRINT_NUMBER_CLR_DIGIT1 = 3        
} ZOBRAZ_CISLO_STATE_e;


void print_segments(uint8_t s){
    PORT_PinWrite(SEGMENT_A, (s >> 0) & 1);
    PORT_PinWrite(SEGMENT_B, (s >> 1) & 1);
    PORT_PinWrite(SEGMENT_C, (s >> 2) & 1);
    PORT_PinWrite(SEGMENT_D, (s >> 3) & 1);
    PORT_PinWrite(SEGMENT_E, (s >> 4) & 1);
    PORT_PinWrite(SEGMENT_F, (s >> 5) & 1);
    PORT_PinWrite(SEGMENT_G, (s >> 6) & 1);
    PORT_PinWrite(SEGMENT_H, (s >> 7) & 1);
}

const uint8_t segments_hexnum[16] = {0b00111111, 0b00000110, 91, 79, 102, 109, 125, 7, 127, 111, 119, 124, 57, 94, 121, 113};

void print_number(uint16_t rad){
    static ZOBRAZ_CISLO_STATE_e _state = PRINT_NUMBER_SET_DIGIT0;
    
    rad = rad / 100;
    
    uint8_t digit0 = rad % 10;
    uint8_t digit1 = rad / 10;
    
    if(digit1 > 9){
        PORT_PinWrite(DIGIT_0, LOW);
        PORT_PinWrite(DIGIT_1, LOW);
        print_segments(segments_hexnum[14]);
    }
    else{
        switch(_state){
            case PRINT_NUMBER_SET_DIGIT0:
                PORT_PinWrite(DIGIT_0, LOW);
                print_segments(segments_hexnum[digit0]);
                _state = PRINT_NUMBER_CLR_DIGIT0;
            break;

            case PRINT_NUMBER_CLR_DIGIT0:
                PORT_PinWrite(DIGIT_0, HIGH);
                print_segments(0b00000000);
                _state = PRINT_NUMBER_SET_DIGIT1;
            break;

            case PRINT_NUMBER_SET_DIGIT1:
                PORT_PinWrite(DIGIT_1, LOW);
                print_segments(segments_hexnum[digit1] | 0b10000000);
                _state = PRINT_NUMBER_CLR_DIGIT1;
            break;

            case PRINT_NUMBER_CLR_DIGIT1:
                PORT_PinWrite(DIGIT_1, HIGH);
                print_segments(0b00000000);
                _state = PRINT_NUMBER_SET_DIGIT0;
            break;
        }
    }
}


int main() {
    
    // Initialization and setup
    
    PORT_PinOutputEnable(SEGMENT_A); // nastaveni pinu pro segmenty jako vystupni
    PORT_PinOutputEnable(SEGMENT_B);
    PORT_PinOutputEnable(SEGMENT_C);
    PORT_PinOutputEnable(SEGMENT_D);
    PORT_PinOutputEnable(SEGMENT_E);
    PORT_PinOutputEnable(SEGMENT_F);
    PORT_PinOutputEnable(SEGMENT_G);
    PORT_PinOutputEnable(SEGMENT_H);
    
    PORT_PinOutputEnable(DIGIT_0);
    PORT_PinOutputEnable(DIGIT_1);
    
    PORT_PinInputEnable(GCIN); // nastaveni pinu Geiger-Muller citace jako vstupni
    
    PORT_PinOutputEnable(PLED1); // nastaveni pinu signalizacni LED diody jako vystupni
    
    PORT_PinWrite(PLED1, LOW); // nastaveni LED didy na 0V
    
    TCB2_Initialize();
    
    SPSE_IOT_GlobalInterruptsEnable();
    
    
    while(1){
        
        // Infinite loop
        
        uint64_t time = TCB2_GetMillis();
        
        static uint8_t turn_on_sound = 0;
        
        
        static uint16_t geiger_counter = 0;
        
        if(PORT_PinRead(GCIN) == LOW){
            ++geiger_counter;
        }
        
        
        static uint64_t next_geiger_time = 0;
        static uint16_t radiation = 0;
        
        if(time >= next_geiger_time){
            next_geiger_time = time + 1000;
            radiation = geiger_counter * 34.2;
            if(radiation > RADIATION_SOUND_ENABLE){
                turn_on_sound = 1;
            }
            geiger_counter = 0;
        }
        
        
        static uint64_t next_display_time = 0;
        
        if(time >= next_display_time){
            next_display_time = time + 1;
            print_number(radiation);
        }
        
    }
    
    return 0; // End of the program
}
